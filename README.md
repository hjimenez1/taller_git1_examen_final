# Prueba final Taller git 1
## Indicaciones 
`1.- Hacer FORK a este repositorio ('fork'='bifurcación')`

`2.- Agregar a jguerrag@canvia.com como colaborador del proyecto bifurcado`

`3.- Compartir url(https) del repositorio bifurcado por teams`

`4.- Clonar su repositorio fork en su local`

`5.- Crean una rama local con su userid`

`6.- Verificar que esten trabajando en su rama creada`

`7.- Completan el test 'Examen final GIT 1.txt'`

`8.- Realizan el commit y luego suben su rama userid a su propio repositorio bifurcado`

`9.- Comunican por teams que ya terminaron.`

