1.- ¿En que año fue creado GIT?
2.- ¿Quien es reconocido como el creador de GIT?
3.- ¿Que comando puedo usar para listar la configuracion global de GIT instalado en mi PC?
4.- ¿Que comando puedo usar para incializar un repositorio local?
5.- Porfavor liste en orden las 3 lineas de comando necesario para realizar un commit simple.
	RESP:
		> git ...
		> git ...
		> git ...

6.- ¿Que comando usamos para listar el historial de commits?
7.- Por favor escriba el comnado para clonar un repositorio remoto.
8.- ¿Como se llama el "apuntador" que nos indica donde estamos posicionados en nuestro repositorio?
9.- Si intentamos borrar una rama local y obtenemos el siguiente mensaje 
			>error: The branch 'mi_rama' is not fully merged.
    Si estamos seguros de querer borrar dicha rama ¿que comando usamos?	

10.- Indique las lineas de comando para crear una rama y posicionarme en esta.
11.- ¿Para que nos sirve el REAMDE.md?
12.- ¿Para que usamos el archivo .gitignore?
13.- Si nos piden que resolvamos un problema en produccion cuando estamos agregando nueva funcionalidad
     al proyecto, que comando podemos usar para guardar temporalmente nuestros avances?

14.- Indique solo un tipo de flujo de trabajo distribuido
15.- Indique cuales son las ramas princiaples para gitflow
16.- Indique cuales son las ramas de apoyo en gitflow

